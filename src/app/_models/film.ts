import { Injectable } from '@angular/core';

import { Adapter } from './adapter';

export class Film {
  constructor(
    public title: number,
    public episode: number,
    public director: string,
    public opening_crawl: string
  ) { }
}


@Injectable()
export class FilmAdapter implements Adapter<Film> {

  adapt(item: any): Film {
    return new Film(
      item.title,
      item.episode_id,
      item.director,
      item.opening_crawl
    );
  }
}
