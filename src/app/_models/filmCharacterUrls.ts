import { Injectable } from '@angular/core';

import { Adapter } from './adapter';

export class FilmCharacterUrls {
  constructor(
    public characters: string
  ) { }
}


@Injectable()
export class FilmCharacterUrlsAdapter implements Adapter<FilmCharacterUrls> {

  adapt(item: any): FilmCharacterUrls {
    return new FilmCharacterUrls(
      item.characters
    );
  }
}
