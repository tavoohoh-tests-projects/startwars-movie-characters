import { Injectable } from '@angular/core';

import { Adapter } from './adapter';

export class Character {
  constructor(
    public name: string,
    public eye_color: string,
    public gender: string,
    public films: string[]
  ) { }
}


@Injectable()
export class CharacterAdapter implements Adapter<Character> {

  adapt(item: any): Character {
    return new Character(
      item.name,
      item.eye_color,
      item.gender,
      item.films
    );
  }
}
