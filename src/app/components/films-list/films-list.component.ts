import { Component, OnInit } from '@angular/core';
import { StarwarsService } from 'app/services/starwars.service';
import { Film } from 'app/_models';

@Component({
  selector: 'app-films-list',
  templateUrl: './films-list.component.html',
  styleUrls: ['./films-list.component.css']
})
export class FilmsListComponent implements OnInit {
  public films: Film[];
  public opening: number;

  constructor(
    private starwarsService: StarwarsService
  ) { }

  ngOnInit() {
    this.getFilmsList();
  }

  /**
   * Get a list of films
   */
  getFilmsList() {
    this.starwarsService.getFilmsApi().subscribe(data => {
      this.films = data;
    });
  }

  /**
   * Show opening crawl for selected film
   * @param episode episode id used as index
   */
  openingCrawl(episode: number) {
    this.opening = episode;
  }

  /**
   * Hide any opening crawl
   */
  openingCrawlStop() {
    this.opening = null;
  }

}
