import { Component, OnInit } from '@angular/core';
import { StarwarsService } from 'app/services/starwars.service';

import { Character, Film } from 'app/_models';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.css']
})
export class CharacterListComponent implements OnInit {

  public characters: Character[] = [];
  public filterValue: string;
  public film: string;

  constructor(
    private route: ActivatedRoute,
    private starwarsService: StarwarsService
  ) { }

  ngOnInit() {
    this.film = this.route.snapshot.paramMap.get('film');

    this.getCharactersList();
    this.getFilmsList();
  }

  /**
   * Get a list of films
   */
  getFilmsList() {
    this.starwarsService.getFilmsApi().subscribe(data => {
      const films = data;
      const filmsArray = [];
      let processed = 0;

      // Get characters and push them to an array of characters
      films.forEach(url => {
        this.starwarsService.getCharactersByUrlApi(url).subscribe(film => {
          processed++;
          filmsArray.push(film['title']);
          if (processed === films.length) {
            this.characters = filmsArray;
          }
        });
      });
    });
  }

  /**
   * Get a list of characters by iterating its urls.
   * NOTE: Could not find an api to filter between the characters in a film.
   */
  getCharactersList() {
    const filmId = this.route.snapshot.paramMap.get('id');

    // Get character urls
    this.starwarsService.getFilmsCharacterUrlsApi(filmId).subscribe(data => {
      const characters = data;
      const charactersArray: Character[] = [];
      let charsProcessed = 0;

      // Get characters and push them to an array of characters
      characters.forEach(url => {
        this.starwarsService.getCharactersByUrlApi(url).subscribe(character => {
          charsProcessed++;
          charactersArray.push(character);
          if (charsProcessed === characters.length) {
            this.characters = charactersArray;
          }
        });
      });
    });
  }

  /**
   * This was an attempt to show the names
   * of the films in which the character appears.
   *
   * NOTE: It was discarded because it was slowing down the app performance.
   */
  async getFilmName(url) {
    // this makes no sense...
    // const film = await this.starwarsService.getFilmByUrlApi(url);
  }

}
