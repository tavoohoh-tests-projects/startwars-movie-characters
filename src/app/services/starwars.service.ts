import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';

import { Film, FilmAdapter, Character, CharacterAdapter } from 'app/_models';
import { FilmCharacterUrls } from 'app/_models/filmCharacterUrls';

const starwarsAPI = 'https://swapi.co/api/';

export const urls = {
  API_FILMS: starwarsAPI + 'films',
  API_CHARACTERS: starwarsAPI + 'people',
}

@Injectable()
export class StarwarsService {

  constructor(
    private http: HttpClient,
    private filmAdapter: FilmAdapter,
    private characterAdapter: CharacterAdapter
  ) { }

  /**
   * Get all films
   */
  getFilmsApi(): Observable<Film[]> {
    return this.http.get(urls.API_FILMS).pipe(
      map((data: any[]) => data['results'].map(item => this.filmAdapter.adapt(item))),
    );
  }

  /**
   * Get all characters url of a film
   * @param filmId film episode id
   */
  getFilmsCharacterUrlsApi(filmId): Observable<FilmCharacterUrls[]> {
    const url = urls.API_FILMS + '/' + filmId
    return this.http.get(url).pipe(
      map((data: any[]) => data['characters']),
    );
  }

  /**
   * Get all characters
   */
  getCharactersApi(): Observable<Character[]> {
    return this.http.get(urls.API_FILMS).pipe(
      map((data: any[]) => data['results'].map(item => this.characterAdapter.adapt(item))),
    );
  }

  /**
   * Get a character from its url
   * @param url string
   */
  getCharactersByUrlApi(url): Observable<Character> {
    return this.http.get(url).pipe(map(data => {
      const character = new Character(
        data['name'],
        data['eye_color'],
        data['gender'],
        data['films']
      );

      return character;
    }));
  }

  /**
   * Get a film from its url
   * @param url string
   */
  getFilmByUrlApi(url): Observable<{title: string}> {
    return this.http.get(url).pipe(map(data => {
      const film = {
        title: data['title']
      }

      return film;
    }));
  }

}
