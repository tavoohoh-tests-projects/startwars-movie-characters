import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

/* external libraries */
import { NgxPaginationModule } from 'ngx-pagination';

/* components */
import { AppComponent } from './app.component';
import { FilmsListComponent } from './components/films-list/films-list.component';
import { CharacterListComponent } from './components/character-list/character-list.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';

/* services */
import { StarwarsService } from './services/starwars.service';

/* models */
import { FilmAdapter, CharacterAdapter, FilmCharacterUrlsAdapter } from './_models';

/* pipes */
import { CharacterFilterPipe } from './_pipes/character-filter.pipe';

const routes: Routes = [
  {
    path: 'home',
    component: FilmsListComponent,
  },
  {
    path: 'characters',
    component: CharacterListComponent,
  },
  {
    path: 'characters/:film/:id',
    component: CharacterListComponent,
  },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  declarations: [
    AppComponent,
    FilmsListComponent,
    CharacterListComponent,
    HeaderComponent,
    FooterComponent,
    CharacterFilterPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    RouterModule.forRoot(routes),
  ],
  providers: [
    StarwarsService,
    FilmAdapter,
    FilmCharacterUrlsAdapter,
    CharacterAdapter
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
